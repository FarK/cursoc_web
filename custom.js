function animated_scroll() {
	$(function() {
		$("a[href='#begin']").on('click', function(e) {
			e.preventDefault();
			$('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
		});
	});
}

function set_icons_title() {
	/* Set 'desc' attribute from 'title' atribute. Later icons
				takes this to its description */
	$('[data-toggle="tooltip"]').each(function() {
		$(this).parent().attr('desc', $(this).attr('title'));
		<!-- $(this).attr('title', $(this).attr('desc')); -->
	});
	$('[data-toggle="tooltip"]').tooltip();
}

function collapse_temario() {
	/* Temario starts collapsed on mobile devices */
	console.log($(window).width());
	if ($(window).width() < 767) {
		$('#listatemario').removeClass('in');
	}
}

function calendar(month, year, marks) {
	var month_names = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

	var first_day_date = new Date(year, month - 1, 1);
	var last_day_date  = new Date(year, month, 0);

	var first_week_day = first_day_date.getDay() - 1;
	if (first_week_day < 0)
		first_week_day = 6;

	var month_days = last_day_date.getDate();
	var last_week_day = last_day_date.getDay() - 1;
	if (last_week_day < 0)
		last_week_day = 6;

	// Create all days cells
	var days = [];
	for (i = 0; i < first_week_day; i++)
		days[i] = "<td class=\"cal-prev\"></td>";

	for (day = 1; day <= month_days; day++)
		days.push("<td class=\"day day-" + day + "\">" + day + "</td>");

	for (i = last_week_day; i < 7; i++)
		days.push("<td class=\"cal-post\"></td>");

	// Create all week rows
	var num_weeks = days.length / 7 - 1;
	var days_table = "";
	var day = 0;
	for (week = 0; week < num_weeks; week++) {
		days_table += "<tr>\n\t";
		for (i = 0; i < 7; i++)
			days_table += days[day++];
		days_table += "\n</tr>\n";
	}

	return "" +
		"<table class=\"table calendar table-responsive\"><tbody>" +
			"<tr class=\"month_name\"><th colspan=7>" + month_names[month - 1] + "</th></tr>" +
			"<tr class=\"week_days\"><th>L</th> <th>M</th> <th>X</th> <th>J</th> <th>V</th> <th>S</th> <th>D</th></tr>" +
			days_table +
		"</tbody></table>";
}

function generate_calendars(year) {
	$('div.calendar').each(function() {
		$(this).html(
			calendar($(this).attr('month'), year)
		);
	});
}

function disable_map_scroll() {
	/* Disable scroll on map until click */
	$('.map').click(function () {
		$('.map iframe').css("pointer-events", "auto");
	});
	$( ".map" ).mouseleave(function() {
		$('.map iframe').css("pointer-events", "none");
	});
}

function normalize_carousel_heights() {
	/* TODO: on window resize */
	var max_height = -1;
	$(".carousel .item").each(function() {
		var h = $(this).height();
		max_height = h > max_height ? h : max_height;
	});
	$(".carousel .item").height(max_height);
}

function back_to_top_button() {
	/* Back to top */
	$(window).scroll(function () {
		if ($(this).scrollTop() <= 200) {
			$('#backToTop').fadeOut();
		} else {
			$('#backToTop').fadeIn();
		}
	});
	$('#backToTop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});
}
