function calendar(month, year, marks) {
	var month_names = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

	var first_day_date = new Date(year, month - 1, 1);
	var last_day_date  = new Date(year, month, 0);

	var first_week_day = first_day_date.getDay() - 1;
	if (first_week_day < 0)
		first_week_day = 6;

	var month_days = last_day_date.getDate();
	var last_week_day = last_day_date.getDay() - 1;
	if (last_week_day < 0)
		last_week_day = 6;

	// Create all days cells
	var days = [];
	for (i = 0; i < first_week_day; i++)
		days[i] = "<td class=\"cal-prev\"></td>";

	for (day = 1; day <= month_days; day++)
		days.push("<td class=\"day day-" + day + "\">" + day + "</td>");

	for (i = last_week_day; i < 7; i++)
		days.push("<td class=\"cal-post\"></td>");

	// Create all week rows
	var num_weeks = days.length / 7 - 1;
	var days_table = "";
	var day = 0;
	for (week = 0; week < num_weeks; week++) {
		days_table += "<tr>\n\t";
		for (i = 0; i < 7; i++)
			days_table += days[day++];
		days_table += "\n</tr>\n";
	}

	return "" +
		"<table class=\"table calendar table-responsive\"><tbody>" +
			"<tr class=\"month_name\"><th colspan=7>" + month_names[month - 1] + "</th></tr>" +
			"<tr class=\"week_days\"><th>L</th> <th>M</th> <th>X</th> <th>J</th> <th>V</th> <th>S</th> <th>D</th></tr>" +
			days_table +
		"</tbody></table>";
}
